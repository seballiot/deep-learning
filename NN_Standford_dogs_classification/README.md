# Projet : Image classification à l’aide du Deep Learning

### Énoncé

[NN_Standford_dogs_classification/Cahier des charges.pdf](https://gitlab.com/seballiot/deep-learning/-/blob/master/NN_Standford_dogs_classification/Cahier%20des%20charges.pdf)

### Dataset

http://vision.stanford.edu/aditya86/ImageNetDogs/

### Réalisation 

- 1 notebook ([1_cropping.ipynb](https://gitlab.com/seballiot/deep-learning/-/blob/master/NN_Standford_dogs_classification/1_cropping.ipynb)) pour EDA et réalisation d'un cropping custom sur les images
- 1 notebook ([2_Custom_CNN.ipynb](https://gitlab.com/seballiot/deep-learning/-/blob/master/NN_Standford_dogs_classification/2_Custom_CNN.ipynb)) pour le **CNN maison** (preprocessing, data augmentation, construction et entraintement du CNN, évaluation et save du modèle)
- 1 notebook ([3_TransferLearning_InceptionV3.ipynb](https://gitlab.com/seballiot/deep-learning/-/blob/master/NN_Standford_dogs_classification/3_TransferLearning_InceptionV3.ipynb)) pour le **CNN par Transfert Learning** (preprocessing, data augmentation, construction et entraintement du CNN, évaluation et save du modèle)
- 1 notebook ([4_predictions.ipynb](https://gitlab.com/seballiot/deep-learning/-/blob/master/NN_Standford_dogs_classification/4_predictions.ipynb)) pour la réalisation des prédictions (API dog + images en local) à partir du 2ème modèle
